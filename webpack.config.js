const path = require('path');

module.exports = {

    module: {
        rules: [
            {
                test: /\.html$/,
                exclude: /node_modules/,
                loader: 'file-loader'
            },
            {
                test: /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],

                use: [
                    {
                        loader: 'elm-webpack-loader',
                        options: {
                            cwd: __dirname,
                            debug: true,
                            verbose: true,
                        }
                    }
                ]
            }
        ]
    },

    mode: 'development',

    devServer: {
        static: {
            directory: path.join(__dirname, "src")
        }
    }
};
